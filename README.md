# shopping-cart

[![pipeline status](https://gitlab.com/kelvinkwan1991/shopping-cart/badges/main/pipeline.svg)](https://gitlab.com/kelvinkwan1991/shopping-cart/commits/main)
[![coverage report](https://gitlab.com/kelvinkwan1991/shopping-cart/badges/main/coverage.svg)](https://gitlab.com/kelvinkwan1991/shopping-cart/commits/main)

A shopping cart program with sales tax calculation and receipt printing

## Design
![](/diagram/shopping-cart-diagram.png)

## Usage
```java
Cart cart = new Cart(Location.CALIFORNIA);
Product book = new Product("book", new BigDecimal("17.99"), Category.STATIONARY);
// add product with quantity
cart.add(book, 1);
Receipt receipt = cart.generateReceipt();
```
You may refer to [UseCase](https://gitlab.com/kelvinkwan1991/shopping-cart/-/blob/main/src/main/java/main/UseCase.java) to run all test cases based on the requirements
