package cart;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import product.Category;
import product.Product;
import receipt.Receipt;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CartTest {
    Cart cart = new Cart(Location.CALIFORNIA);

    Product potatoChips = new Product("potato chips", new BigDecimal("3.99"), Category.FOOD);
    Product book = new Product("book", new BigDecimal("17.99"), Category.STATIONARY);

    @AfterEach
    void emptyCart() {
        cart.emptyCart();
    }

    @Test
    void shouldInitiateAnEmptyCart() {
        List<Item> items = cart.getItems();
        assertThat(items).isEmpty();
    }

    @Test
    void shouldAddItemsInCart() {
        cart.add(book, 2);
        List<Item> items = cart.getItems();

        assertThat(items).hasSize(1).satisfiesExactly(item -> {
            assertThat(item.getProduct().getName()).isEqualTo("book");
            assertThat(item.getQuantity()).isEqualTo(2);
        });
    }

    @Test
    void shouldGenerateReceipt() {
        cart.add(book, 1);
        cart.add(potatoChips, 1);
        Receipt receipt = cart.generateReceipt();
        assertThat(receipt.toString()).isEqualTo(
                "item                          price            qty\n\n" +
                        "book                         $17.99              1\n" +
                        "potato chips                  $3.99              1\n" +
                        "subtotal:                                   $21.98\n" +
                        "tax:                                         $1.80\n" +
                        "total:                                      $23.78\n"
        );
    }

    @Test
    void shouldEmptyCart() {
        cart.emptyCart();
        assertThat(cart.getItems()).isEmpty();
    }
}
