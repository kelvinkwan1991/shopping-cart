package calculator;

import cart.Item;
import cart.Location;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import product.Category;
import product.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTest {
    Product potatoChips = new Product("potato chips", new BigDecimal("3.99"), Category.FOOD);
    Product book = new Product("book", new BigDecimal("17.99"), Category.STATIONARY);
    Product pencil = new Product("pencil", new BigDecimal("2.99"), Category.STATIONARY);
    Product shirt = new Product("shirt", new BigDecimal("29.99"), Category.CLOTHING);
    List<Item> items = new ArrayList<>();
    Calculator caCalculator = Calculator.getInstance(Location.CALIFORNIA);
    Calculator nyCalculator = Calculator.getInstance(Location.NEW_YORK);

    @Test
    void shouldGetCalculatorInstanceByLocation() {
        assertThat(Calculator.getInstance(Location.CALIFORNIA)).isInstanceOf(CACalculator.class);
        assertThat(Calculator.getInstance(Location.NEW_YORK)).isInstanceOf(NYCalculator.class);
    }

    @AfterEach
    public void removeItemsFromList() {
        items.removeAll(items);
    }

    @Test
    void shouldCalculateSubtotalForAllItems() {
        items.add(new Item(book, 1));
        items.add(new Item(potatoChips, 1));
        assertThat(Calculator.calculateSubtotal(items)).isEqualTo("21.98");
    }

    @Test
    void shouldExemptFoodTaxInCalifornia() {
        items.add(new Item(potatoChips, 1));
        assertThat(caCalculator.calculateTax(items)).isZero();
    }

    @Test
    void shouldCalculateItemTaxInCalifornia() {
        items.add(new Item(book, 2));
        assertThat(caCalculator.calculateTax(items)).isEqualTo("3.55");
    }

    @Test
    void shouldExemptFoodAndClothingTaxInNewYork() {
        items.add(new Item(potatoChips, 1));
        items.add(new Item(shirt, 1));
        assertThat(nyCalculator.calculateTax(items)).isZero();
    }

    @Test
    void shouldCalculateItemTaxInNewYork() {
        items.add(new Item(pencil, 2));
        items.add(new Item(shirt, 1));
        assertThat(nyCalculator.calculateTax(items)).isEqualTo("0.55");
    }
}