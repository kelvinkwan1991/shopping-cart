package receipt;

import cart.Item;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import product.Category;
import product.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ReceiptTest {
    Receipt receipt = new ConsoleReceipt();
    Product book = new Product("book", new BigDecimal("17.99"), Category.STATIONARY);
    Product potatoChips = new Product("potato chips", new BigDecimal("3.99"), Category.FOOD);
    List<Item> items = new ArrayList<>();
    BigDecimal amount = new BigDecimal("100.95");

    @AfterEach
    void resetReceipt() {
        receipt = new ConsoleReceipt();
    }

    @Test
    void shouldDisplayItemsDetail() {
        items.add(new Item(book, 1));
        items.add(new Item(potatoChips, 2));
        receipt.appendItems(items);
        assertThat(receipt.toString()).isEqualTo(
                "item                          price            qty\n\n" +
                         "book                         $17.99              1\n" +
                         "potato chips                  $3.99              2\n"
        );
    }

    @Test
    void shouldDisplaySubtotal() {
        receipt.appendSubtotal(amount);
        assertThat(receipt.toString()).isEqualTo("subtotal:                                  $100.95\n");
    }

    @Test
    void shouldDisplayTax() {
        receipt.appendTax(amount);
        assertThat(receipt.toString()).isEqualTo("tax:                                       $100.95\n");
    }

    @Test
    void shouldDisplayTotal() {
        receipt.appendTotal(amount);
        assertThat(receipt.toString()).isEqualTo("total:                                     $100.95\n");
    }
}