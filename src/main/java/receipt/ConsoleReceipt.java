package receipt;

import cart.Item;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ConsoleReceipt implements Receipt {
    private static final String ITEM_HEADER_FORMAT = "%-20s%15s%15s";
    private static final String ITEM_FORMAT = "%-20s%15s%15d";
    private static final String SUMMARY_FORMAT = "%-35s%15s";
    private static final NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
    private static final String[] HEADERS = {"item", "price", "qty"};
    private static final String SUBTOTAL_TITLE = "subtotal:";
    private static final String TAX_TITLE = "tax:";
    private static final String TOTAL_TITLE = "total:";

    private StringBuilder builder = new StringBuilder();

    @Override
    public Receipt appendItems(List<Item> items) {
        appendLine(String.format(ITEM_HEADER_FORMAT, HEADERS));
        appendLine("");
        items.stream().forEach(i -> {
            appendLine(String.format(ITEM_FORMAT,
                    i.getProduct().getName(),
                    numberFormat.format(i.getProduct().getPrice()),
                    i.getQuantity()));
        });
        return this;
    }

    @Override
    public Receipt appendSubtotal(BigDecimal subtotal) {
        appendLine(String.format(SUMMARY_FORMAT, SUBTOTAL_TITLE, numberFormat.format(subtotal)));
        return this;
    }

    @Override
    public Receipt appendTax(BigDecimal tax) {
        appendLine(String.format(SUMMARY_FORMAT, TAX_TITLE, numberFormat.format(tax)));
        return this;
    }

    @Override
    public Receipt appendTotal(BigDecimal total) {
        appendLine(String.format(SUMMARY_FORMAT, TOTAL_TITLE, numberFormat.format(total)));
        return this;
    }

    @Override
    public String toString() {
        return builder.toString();
    }

    private void appendLine(String text){
        builder.append(text);
        builder.append(System.lineSeparator());
    }
}
