package receipt;

import cart.Item;

import java.math.BigDecimal;
import java.util.List;

public interface Receipt {
    Receipt appendItems(List<Item> items);
    Receipt appendSubtotal(BigDecimal subtotal);
    Receipt appendTax(BigDecimal tax);
    Receipt appendTotal(BigDecimal total);
}
