package calculator;

import cart.Item;
import cart.Location;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.ResourceBundle;

public abstract class Calculator {
    public static final BigDecimal ROUNDING_PRECISION = new BigDecimal("0.05");

    public static Calculator getInstance(Location location) {
        ResourceBundle bundle = ResourceBundle.getBundle(location.getBundleName());
        try {
            return (Calculator) Class.forName(location.getClassName())
                    .getDeclaredConstructor(ResourceBundle.class)
                    .newInstance(bundle);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                 | NoSuchMethodException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static BigDecimal calculateSubtotal(List<Item> items) {
        return items.stream()
                .map(i -> i.getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal round(BigDecimal value, BigDecimal increment, RoundingMode roundingMode) {
        if (increment.signum() == 0) return value;

        BigDecimal divided = value.divide(increment, 0, roundingMode);
        BigDecimal result = divided.multiply(increment);
        return result;
    }

    public abstract BigDecimal calculateTax(List<Item> items);
}
