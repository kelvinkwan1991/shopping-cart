package calculator;

import cart.Item;
import product.Category;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NYCalculator extends Calculator {
    private BigDecimal taxRate;
    private Set<Category> categories;

    public NYCalculator(ResourceBundle bundle) {
        this.taxRate = new BigDecimal(bundle.getString("taxRate"));
        categories = Stream.of(bundle.getString("exemptionCategory").split(","))
                .map(s -> Category.valueOf(s.toUpperCase()))
                .collect(Collectors.toSet());
    }

    @Override
    public BigDecimal calculateTax(List<Item> items) {
        return Calculator.round(items.stream()
                .filter(i -> !categories.contains(i.getProduct().getCategory()))
                .map(i -> i.getAmount().multiply(taxRate))
                .reduce(BigDecimal.ZERO, BigDecimal::add), Calculator.ROUNDING_PRECISION, RoundingMode.UP);
    }
}
