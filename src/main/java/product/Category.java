package product;

public enum Category {
    FOOD,
    STATIONARY,
    CLOTHING
}
