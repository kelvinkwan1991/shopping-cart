package main;

import cart.Cart;
import cart.Location;
import product.Category;
import product.Product;
import receipt.Receipt;

import java.math.BigDecimal;

public class UseCase {
    public static final Cart caCart = new Cart(Location.CALIFORNIA);
    public static final Cart nyCart = new Cart(Location.NEW_YORK);
    public static final Product book = new Product("book", new BigDecimal("17.99"), Category.STATIONARY);
    public static final Product potatoChips = new Product("potato chips", new BigDecimal("3.99"), Category.FOOD);
    public static final Product pencil = new Product("pencil", new BigDecimal("2.99"), Category.STATIONARY);
    public static final Product shirt = new Product("shirt", new BigDecimal("29.99"), Category.CLOTHING);

    public static void main(String[] args) {
        useCase1();
        useCase2();
        nyCart.emptyCart();
        useCase3();
    }

    public static void useCase1() {
        caCart.add(book, 1);
        caCart.add(potatoChips, 1);
        Receipt receipt = caCart.generateReceipt();
        System.out.println(receipt);
    }

    public static void useCase2() {
        nyCart.add(book, 1);
        nyCart.add(pencil, 3);
        Receipt receipt = nyCart.generateReceipt();
        System.out.println(receipt);
    }

    public static void useCase3() {
        nyCart.add(pencil, 2);
        nyCart.add(shirt, 1);
        Receipt receipt = nyCart.generateReceipt();
        System.out.println(receipt);
    }
}
