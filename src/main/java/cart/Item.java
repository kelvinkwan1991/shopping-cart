package cart;

import lombok.AllArgsConstructor;
import lombok.Getter;
import product.Product;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class Item {
    private Product product;
    private int quantity;

    public BigDecimal getAmount() {
        return product.getPrice().multiply(new BigDecimal(quantity));
    }
}
