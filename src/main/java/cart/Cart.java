package cart;

import calculator.Calculator;
import product.Product;
import receipt.ConsoleReceipt;
import receipt.Receipt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Item> items = new ArrayList<>();
    private Calculator calculator;

    public Cart(Location location) {
        calculator = Calculator.getInstance(location);
    }

    public void add(Product product, int quantity) {
        items.add(new Item(product, quantity));
    }

    public List<Item> getItems() {
        return items;
    }

    public void emptyCart() {
        items.removeAll(items);
    }

    public Receipt generateReceipt() {
        BigDecimal subtotal = Calculator.calculateSubtotal(items);
        BigDecimal tax = calculator.calculateTax(items);
        Receipt receipt = new ConsoleReceipt()
                .appendItems(items)
                .appendSubtotal(subtotal)
                .appendTax(tax)
                .appendTotal(subtotal.add(tax));
        return receipt;
    }
}
