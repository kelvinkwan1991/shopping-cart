package cart;

public enum Location {
    CALIFORNIA("calculator.CACalculator", "ca"),
    NEW_YORK("calculator.NYCalculator", "ny");

    private String className;
    private String bundleName;

    Location(String className, String bundleName) {
        this.className = className;
        this.bundleName = bundleName;
    }

    public String getClassName() {
        return className;
    }

    public String getBundleName() {
        return bundleName;
    }
}
